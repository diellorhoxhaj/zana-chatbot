import {Timestamp} from 'rxjs';
import {Item} from './Item';

export interface ConversationMessage {
  senderMessage: string;
  senderTime?: any;
  botMessageTime?: any;
  botMessage?: any;
  mainItem?: Item;
  items?: any[];
  context?: string[];
}
