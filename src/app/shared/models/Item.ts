export interface Item {
  URI: string;
  date: string;
  image_url: any;
  payload_link: string;
  payload_title: string;
  source: string;
  subtitle: string;
  summary: string;
  title: string;
  type_template: string;
}
