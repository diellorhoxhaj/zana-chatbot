import {Item} from './item';
export interface Message {
  action: Action;
  context: string[];
  items: Item[]; // string[]
  lang: string;
  message: any;
  msgVoice: string;
  sid: string;
  timestamp: string;
}
export interface Action {
  disableVoiceAndText: boolean;
  label: string;
  name: string;
}
export interface UserSessionMessage {
  userId: string;
  botMessages: string[];
}
