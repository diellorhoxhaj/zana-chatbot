import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Message} from '../models/Message';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  baseUrl = '';
  constructor(private http: HttpClient) { }
  getMessage(message, userId, isMedici, context?): Observable<Message> {
    if (isMedici) {
      this.baseUrl = 'https://dev.zana.com/medici1.0/brain/?';
    } else {
      this.baseUrl = 'https://api.zana.com/brain/?';
    }
    let params = new HttpParams();
    params = params.append('u', `${message}`);
    params = params.append('sid', userId);
    params = params.append('context', context);
    params = params.append('lang', 'en');
    return this.http.get<Message>(this.baseUrl, {params});
  }
}
